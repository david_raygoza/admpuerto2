<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 // --------------------------------------------------------------------
    
    /**
     * Get image properties
     *
     * A helper function that gets info about the file
     *
     * @access    public
     * @param    string
     * @return    mixed
     */            
    function get_image_properties($path = '')
    {
        // For now we require GD but we should
        // find a way to determine this using IM or NetPBM
     //   echo 'rerre'.$return;
        if ($path == '')
            $path = $this->full_src_path;

                
       /* if ( ! file_exists($path))
        {
           //$set_error('imglib_invalid_path');       
            echo 'no existe'; 
            return FALSE;                
        }else{
            echo 'no existe';
        }
        */
        $vals = @getimagesize($path);
       // var_dump($vals);
       //die();
        
        $types = array(1 => 'gif', 2 => 'jpeg', 3 => 'png');
        
        $mime = (isset($types[$vals['2']])) ? 'image/'.$types[$vals['2']] : 'image/jpg';
                
        if (is_array($vals))
        {
            $v['width']            = $vals['0'];
            $v['height']        = $vals['1'];
            $v['image_type']    = $vals['2'];
            $v['size_str']        = $vals['3'];
            $v['mime_type']        = $mime;
           // var_dump($v);
            return $v;
        }else{
            echo 'dice que no';
        }
        
      /*  $orig_width    = $vals['0'];
        $orig_height    = $vals['1'];
        $this->image_type    = $vals['2'];
        $this->size_str        = $vals['3'];
        $this->mime_type    = $mime;*/
        
        return TRUE;
    }  
